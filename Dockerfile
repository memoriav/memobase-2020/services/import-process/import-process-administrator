FROM openjdk:8-jre-slim-buster
ADD target/scala-2.12/app.jar /app/app.jar
ADD src/main/resources/app.yml /configs/app.yml
CMD java -jar /app/app.jar
