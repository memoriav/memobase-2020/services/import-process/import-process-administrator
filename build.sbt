import Dependencies._

ThisBuild / scalaVersion := "2.12.11"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}


lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Import Process Administrator",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
    mainClass in assembly := Some("ch.memobase.App"),
    resolvers ++= Seq(
      "Memobase Utils" at "https://dl.bintray.com/memoriav/memobase"
    ),
    libraryDependencies ++= Seq(
      kafkaStreams,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils,
      uPickle,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test,
      scalatic % Test)
  )
