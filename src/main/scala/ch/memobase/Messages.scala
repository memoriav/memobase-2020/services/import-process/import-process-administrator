/*
 * import-process-administrator
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import upickle.default.{Reader, macroRW}


/**
 * Terminates a Kafka service when sent downstream
 *
 * @param processId Id of import process
 * @param jobName   Name of job inside process
 */
case class ServiceTermination(processId: String, jobName: String) {
  override def toString: String = s"""{"action":"termination","processId":"$processId","job-name":"$jobName"}"""
}

// FIXME: Can eventually be removed
/**
 *
 * @param id      Record id
 * @param status  Processing result; either SUCCESS or FAILURE
 * @param message Message in case of error
 */
case class ServiceReport(id: String, status: String, message: String)

object ServiceReport {

  implicit val reader: Reader[ServiceReport] = macroRW

  def apply(msg: String): ServiceReport = {
    val json = upickle.default.read[ServiceReport](msg)
    new ServiceReport(json.id, json.status, json.message)
  }
}

/**
 * Declares the size of a data set to be expected by a job in an import process
 *
 * @param processId   Id of import process
 * @param jobName     Name of job inside process
 * @param dataSetSize Size of data set
 */
case class DataSetSize(processId: String, jobName: String, dataSetSize: Long)

object DataSetSize {

  implicit val reader: Reader[DataSetSize] = macroRW

  def apply(msg: String): DataSetSize = {
    val json = upickle.default.read[DataSetSize](msg)
    new DataSetSize(json.processId, json.jobName, json.dataSetSize)
  }
}
