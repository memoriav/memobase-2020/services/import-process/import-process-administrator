/*
 * import-process-administrator
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.kstream.Transformer
import org.apache.kafka.streams.processor.ProcessorContext

class ReportTransformer extends Transformer[String, String, KeyValue[String, Int]] {
  private var context: ProcessorContext = _

  override def init(context: ProcessorContext): Unit =
    this.context = context

  override def transform(key: String, value: String): KeyValue[String, Int] =
    new KeyValue[String, Int](context.topic().replace(SettingsFromFile.getReportingInputTopicPostfix, ""), 1)

  override def close(): Unit = {}
}
