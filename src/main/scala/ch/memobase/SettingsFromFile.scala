/*
 * import-process-administrator
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.Properties

import org.memobase.settings.SettingsLoader

import scala.collection.JavaConverters._

trait Settings {
  def getKafkaStreamsSettings: Properties

  def getReportingInputTopicPostfix: String

  def getCountInputTopic: String

  def getKafkaOutputTopic: String

  def getKafkaReportTopic: String

  def getId: String
}


/**
 * Acts as a single point of fetching for the SettingsLoader
 */
object SettingsFromFile extends Settings {
  private val settings = new SettingsLoader(List(
    "countTopicIn"
  ).asJava,
    "app.yml",
    false,
    true,
    false,
    false)

  def getKafkaStreamsSettings: Properties = settings.getKafkaStreamsSettings

  def getReportingInputTopicPostfix: String = settings.getInputTopic

  override def getCountInputTopic: String = settings.getAppSettings.getProperty("countTopicIn")

  def getKafkaOutputTopic: String = settings.getOutputTopic

  def getKafkaReportTopic: String = settings.getProcessReportTopic

  def getId: String = "import-process-administrator"
}
