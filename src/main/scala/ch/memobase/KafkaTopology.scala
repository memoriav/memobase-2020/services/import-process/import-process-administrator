/*
 * import-process-administrator
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.regex.Pattern

import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Materialized
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.logging.log4j.scala.Logging

import scala.util.{Success, Try}


class KafkaTopology extends Logging {

  import Serdes._


  def build(): Topology = {
    val intermediaryDataSetCountTopic = "import-process-filtered-dataset-count"
    val intermediaryReportingTopic = "import-process-ordered-service-reports"
    val builder = new StreamsBuilder

    val topicPattern = Pattern.compile(s".*${SettingsFromFile.getReportingInputTopicPostfix}" + "$")

    val reportSource = builder.stream[String, String](topicPattern)
    val orderedReportSource = builder.stream[String, Int](intermediaryReportingTopic)

    val dataSetSizeRegistry = builder.stream[String, String](SettingsFromFile.getCountInputTopic)

    dataSetSizeRegistry
      .flatMap(convertToLongValue)
      .to(intermediaryDataSetCountTopic)

    val aggregatedReports = builder
      .globalTable(intermediaryDataSetCountTopic,
        Materialized.as[String, Long, ByteArrayKeyValueStore]("reporting-counts-store")
          .withKeySerde(Serdes.String)
          .withValueSerde(Serdes.Long)
      )

    reportSource
      .transform(() => new ReportTransformer)
      .to(intermediaryReportingTopic)

    orderedReportSource
      .groupByKey
      .count
      .toStream
      .join(aggregatedReports)(mapKey, allRecordsProcessed)
      .filter((_, v) => v)
      .map((k, _) => (k, ServiceTermination(getProcessId(k), getJobId(k)).toString))
      .to(SettingsFromFile.getKafkaOutputTopic)

    builder.build()
  }

  private def getProcessId(s: String) = s.split("-")(0)

  private def getJobId(s: String) = s.split("-")(1)

  private def convertToLongValue(k: String, v: String): List[(String, Long)] = Try(v.toLong) match {
    case Success(l) =>
      logger.info(s"Receiving new request to register dataset count: $k -> $v")
      List((k, l))
    case _ =>
      logger.warn("Ignoring message since count is no valid value")
      List()
  }

  private def mapKey(k: String, _v: Long): String = k

  private def allRecordsProcessed(leftValue: Long, rightValue: Long): Boolean = {
    if (leftValue >= rightValue) {
      logger.info(s"""$leftValue of $rightValue processed => all records processed""")
    } else {
      logger.debug(s"""$leftValue of $rightValue processed => still ${rightValue - leftValue} records to be processed""")
    }
    leftValue >= rightValue
  }
}
